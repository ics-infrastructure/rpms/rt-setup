rt-setup
========

The 'rt-setup' package configures details required by RHEL-RT environment.
It is required by the kernel-rt RPM.

To build a new version of the RPM:

  - update the TAG variable in the build.sh script
  - push your changes to check that the RPM can be built by gitlab-ci
  - tag the repository and push the tag for the RPM to be uploaded to artifactory rpm-ics repo
