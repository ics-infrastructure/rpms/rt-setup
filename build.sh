#!/bin/bash
set -e

# Define the tag to use
TAG="imports/c7-rt/rt-setup-2.0-9.el7"

git clone https://git.centos.org/git/centos-git-common.git
git clone https://git.centos.org/git/rpms/rt-setup

cd rt-setup
git checkout ${TAG} && ../centos-git-common/get_sources.sh -b c7-rt
rpmbuild --define "%_topdir $(pwd)" --target x86_64 -ba SPECS/rt-setup.spec
tree RPMS
